#!/bin/bash
_pwd=$(pwd)

find ./bundle/* -prune -type d | while read -r dir
do
  cd "${dir}" || exit 0
  echo "↳ change to \"${dir}\""
  git pull --verbose --stat
  cd "${_pwd}" || exit 0
  echo "↰ ${_pwd}"
done

unset _pwd
