#!/bin/bash
# for each bundle directory, get unique remote url, w/o the \.git$ extension
find ./bundle/* -prune -type d | while read -r dir
do
  cd "${dir}" || exit 0
  git remote --verbose | awk '{print $2}' | sort -u | sed -E 's/\.git$//g'
done
