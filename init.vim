" Mike.Stilson@vml.com
" ~/.config/nvim/init.vim
let g:loaded_perl_provider = 0   " disable
let g:loaded_python_provider = 0 " disable
let g:loaded_ruby_provider = 0   " disable
let g:python3_host_prog = '/opt/local/bin/python'
" o pathogen
" * {{{
" |o plugin preload checks
" |- {{{
let g:pathogen_blacklist = []
" comment/uncomment to enable/disable
" call add(g:pathogen_blacklist, 'NeoSolarized')
" call add(g:pathogen_blacklist, 'ale')
call add(g:pathogen_blacklist, 'coc.nvim')
" call add(g:pathogen_blacklist, 'ctrlp.vim')
call add(g:pathogen_blacklist, 'deoplete-ternjs')
call add(g:pathogen_blacklist, 'deoplete.nvim')
call add(g:pathogen_blacklist, 'echodoc.vim')
" call add(g:pathogen_blacklist, 'editorconfig-vim')
" call add(g:pathogen_blacklist, 'lightline-ale')
" call add(g:pathogen_blacklist, 'lightline-bufferline')
" call add(g:pathogen_blacklist, 'lightline.vim')
" call add(g:pathogen_blacklist, 'scss-syntax.vim')
call add(g:pathogen_blacklist, 'tern_for_vim')
call add(g:pathogen_blacklist, 'vdebug')
" call add(g:pathogen_blacklist, 'vim-commentary')
" call add(g:pathogen_blacklist, 'vim-css3-syntax')
" call add(g:pathogen_blacklist, 'vim-devicons')
" call add(g:pathogen_blacklist, 'vim-fugitive')
" call add(g:pathogen_blacklist, 'vim-gitgutter')
" call add(g:pathogen_blacklist, 'vim-javascript')
" call add(g:pathogen_blacklist, 'vim-jsx-pretty')
" call add(g:pathogen_blacklist, 'vim-repeat')
" call add(g:pathogen_blacklist, 'vim-scriptease')
" call add(g:pathogen_blacklist, 'vim-sensible')
" call add(g:pathogen_blacklist, 'vim-surround')
" |- }}}
" spread pathogen ...
execute pathogen#infect()
execute pathogen#helptags()
colorscheme NeoSolarized
filetype plugin indent on
syntax on
set background=dark
set clipboard+=unnamedplus
set nocompatible
set sessionoptions-=options
set termguicolors
" |o plugins
" |- {{{
" |>o ale
" |>- {{{
" |>-o additional config
" |>-- {{{
" map Ctrl-j / Ctrl-k to next / previous error and wrap on beginning/end
nmap <silent> <C-j> <Plug>(ale_next_wrap)
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
" let g:ale_completion_autoimport = 1
" let g:ale_completion_enabled = 1
let g:ale_fix_on_save = 0
let g:ale_lint_on_enter = 0
" ignore minified
let b:ale_pattern_options = {
  \ '\.(bundle|min)\.css$': {
  \   'ale_linters': [],
  \   'ale_fixers': []
  \   },
  \ '\.(bundle|min)\.js$': {
  \   'ale_linters': [],
  \   'ale_fixers': []
  \   },
  \ 'public\/css\/app\.css$': {
  \   'ale_linters': [],
  \   'ale_fixers': []
  \   },
  \ 'public\/js\/app\.js$': {
  \   'ale_linters': [],
  \   'ale_fixers': []
  \   },
  \ }
" fer funsies
let g:ale_sign_error = 'ﮏ'
let g:ale_sign_warning = ''
" |>-- }}}
" |>-o fixers and linters
" |>-- {{{
let g:ale_fixers = {
  \ '*': ['remove_trailing_lines', 'trim_whitespace'],
  \ 'css': ['stylelint'],
  \ 'html': ['tidy'],
  \ 'javascript': ['eslint'],
  \ 'json': ['prettier'],
  \ 'javascriptreact': ['eslint'],
  \ 'less': ['stylelint'],
  \ 'markdown': ['prettier'],
  \ 'php': ['phpcbf'],
  \ 'sass': ['stylelint'],
  \ 'scss': ['stylelint'],
  \ 'typescript': ['eslint'],
  \ 'xml': ['xmllint', 'prettier'] }
" let g:ale_linter_aliases = {
"   \ 'jsx': ['javascript'] }
let g:ale_linters = {
  \ 'css': ['stylelint'],
  \ 'html': ['tidy'],
  \ 'javascript': ['eslint'],
  \ 'json': ['jq'],
  \ 'javascriptreact': ['eslint'],
  \ 'less': ['stylelint'],
  \ 'markdown': ['prettier', 'proselint'],
  \ 'php': ['phpcs'],
  \ 'sass': ['stylelint'],
  \ 'scss': ['stylelint'],
  \ 'sh': ['shellcheck'],
  \ 'typescript': ['eslint'],
  \ 'xml': ['xmllint'] }
let g:ale_html_tidy_options = '-config ~/.tidyrc -imq'
" potentially handy templates
"let g:ale_javascript_eslint_executable = 'npx eslint'
"let g:ale_javascript_prettier_executable = 'npx prettier'
"let g:ale_php_phpcbf_standard = 'Drupal,PSR2'
"let g:ale_php_phpcs_standard = 'Drupal,PSR2'
"let g:ale_typescript_tslint_executable = '~/.nvm/versions/node/vX.Y.Z/bin/tslint'
"let g:ale_typescript_tsserver_executable = '~/.nvm/versions/node/vX.Y.Z/bin/tsserver'
" |>-- }}}
" |>- }}}
" |>o ctrlp.vim
" |>- {{{
" default ignores
let g:ctrlp_custom_ignore = {
\  'dir':  '\v[\/](\.(bzr|git|hg|svn)|\_site|Library|Pictures)$',
\  'file': '\v\.(bz2|class|db|dmg|gif|gz|jpg|jpeg|pkg|png|so|tar|ttf|xip|zip)$',
\}
" use leader instead of actual named binding
nmap <leader>p :CtrlP<cr>
" bindings for various modes: buffer, mixed and recent
nmap <leader>pb :CtrlPBuffer<cr>
nmap <leader>pm :CtrlPMixed<cr>
nmap <leader>pr :CtrlPMRU<cr>
" allow modified buffers to be hidden
set hidden
" |>- }}}
" |>o deoplete/tern
" |>- {{{
" set completeopt=longest,menuone,preview
" let g:deoplete#enable_at_startup = 1
" call deoplete#custom#option("sources", {
" \ "_": ["ale", "ternjs"],
" \})
" call deoplete#custom#option("omni", "functions", {
" \ "_": ["tern#Complete"],
" \})
" let g:deoplete#sources#ternjs#depths = 1
" let g:deoplete#sources#ternjs#docs = 1
" let g:deoplete#sources#ternjs#filter = 0
" let g:deoplete#sources#ternjs#guess = 0
" let g:deoplete#sources#ternjs#include_keywords = 1
" let g:deoplete#sources#ternjs#filetypes = [
"   \ "jsx",
"   \ "javascript.jsx",
"   \ "vue"
"   \ ]
" let g:deoplete#sources#ternjs#tern_bin = "tern"
" let g:deoplete#sources#ternjs#timeout = 1
" let g:deoplete#sources#ternjs#types = 1
" let g:tern#command = ["tern"]
" let g:tern#arguments = ["--persistent"]
" |>- }}}
" |>o editorconfig-vim
" |>- {{{
" play nice with vim-fugitive; don't open files over ssh
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']
let g:EditorConfig_exec_path = '/opt/local/bin/editorconfig'
" |>- }}}
" |>o lightline.vim
" |>- {{{
set noshowmode     " turn default mode indicator off
set showtabline=2  " always show tabline

function! GitBranch()
  if exists('*FugitiveHead')
    let branch = FugitiveHead()
    return branch !=# '' ? ' '.branch : ''
  fi
  return ''
endfunction
function! ReadOnlyCheck()
  return &readonly && &filetype !=# 'help' ? '' : ''
endfunction
let g:lightline#ale#indicator_checking = '﨔'
let g:lightline#ale#indicator_errors = '﨑'
let g:lightline#ale#indicator_ok = '﨓'
let g:lightline#ale#indicator_warnings = ' '
let g:lightline#bufferline#enable_devicons = 1
let g:lightline#bufferline#shorten_path = 0
let g:lightline#bufferline#unicode_symbols = 1
let g:lightline = {
\ 'active': {
\   'left': [
\     [ 'mode', 'paste' ],
\     [ 'readonly', 'gitbranch' ], ],
\   'right': [
\     [ 'lineinfo' ],
\     [ 'linter_checking', 'linter_errors', 'linter_ok', 'linter_warnings' ] ] },
\ 'colorscheme': 'solarized',
\ 'component': {
\   'lineinfo': ' %3l:%-2v' },
\ 'component_expand': {
\   'buffers': 'lightline#bufferline#buffers',
\   'linter_checking': 'lightline#ale#checking',
\   'linter_errors': 'lightline#ale#errors',
\   'linter_ok': 'lightline#ale#ok',
\   'linter_warnings': 'lightline#ale#warnings' },
\ 'component_function': {
\   'gitbranch': 'GitBranch',
\   'readonly': 'ReadOnlyCheck' },
\ 'component_type': {
\   'buffers': 'tabsel',
\   'linter_checking': 'left',
\   'linter_errors': 'error',
\   'linter_ok': 'left',
\   'linter_warnings': 'warning' },
\ 'inactive': {
\   'left': [
\     [ 'gitbranch' ] ],
\   'right': [
\     [ 'lineinfo' ], ] },
\ 'mode_map': {
\   'n' : '呂',
\   'i' : '',
\   'R' : '﯒',
\   'v' : '麗',
\   'V' : '濾',
\   "\<C-v>": '礪',
\   'c' : '',
\   's' : "SELECT",
\   'S' : "S-LINE",
\   "\<C-s>": "S-BLOCK",
\   't': '' },
\ 'tabline': {
\   'left': [
\     ['buffers'], ],
\   'right': [], },
\ }
" |>- }}}
" |>o vdebug
" |>- {{{
  " nnoremap <leader>gs :Gstatus<CR>
  " nnoremap <leader>gc :Gcommit -v -q<CR>
  " nnoremap <leader>ga :Gcommit --amend<CR>
  " nnoremap <leader>gt :Gcommit -v -q %<CR>
  " nnoremap <leader>gd :Gdiff<CR>
  " nnoremap <leader>ge :Gedit<CR>
  " nnoremap <leader>gr :Gread<CR>
  " nnoremap <leader>gw :Gwrite<CR><CR>
  " nnoremap <leader>gl :silent! Glog<CR>
  " nnoremap <leader>gp :Ggrep<Space>
  " nnoremap <leader>gm :Gmove<Space>
  " nnoremap <leader>gb :Git branch<Space>
  " nnoremap <leader>go :Git checkout<Space>
  " nnoremap <leader>gps :Dispatch! git push<CR>
  " nnoremap <leader>gpl :Dispatch! git pull<CR>
" |>- }}}
" |>o vim-gitgutter
" |>- {{{
set updatetime=250                  " default: 4000 (slower)
let g:gitgutter_eager = 0
let g:gitgutter_max_signs = 2048    " default: 500
let g:gitgutter_override_sign_column_highlight = 0
let g:gitgutter_realtime = 0
let g:gitgutter_sign_added = '樂'
let g:gitgutter_sign_modified = ''
let g:gitgutter_sign_removed = ''
let g:gitgutter_sign_removed_first_line = 'ﰣ_'
let g:gitgutter_sign_modified_removed = ''
" |>- }}}
" |- }}}
" * }}}
" o customization
" * {{{
" |o auto commands
" |- {{{
autocmd VimEnter * highlight clear SignColumn
augroup TypeCSS
  autocmd!
  " https://github.com/hail2u/vim-css3-syntax
  autocmd FileType css setlocal iskeyword+=-
augroup END
augroup TypeDrupalPHP
  autocmd!
  autocmd BufNewFile,BufRead *.inc,*.module,*.install,*.profile,*.test,*.theme,*.view setlocal filetype=php
augroup END
" augroup TypeVue
"     autocmd!
"     au BufNewFile,BufRead *.vue set filetype=css.javascript.jsx
" augroup END
augroup TypeMake
  autocmd!
  " don't expand tabs in Makefiles
  autocmd BufNewFile,BufRead Makefile setlocal noexpandtab
augroup END
augroup TypeXMLAEM
  autocmd!
  autocmd BufNewFile,BufRead */.content.xml setlocal expandtab
  autocmd BufNewFile,BufRead */.content.xml setlocal shiftwidth=2
  autocmd BufNewFile,BufRead */.content.xml setlocal softtabstop=2
  autocmd BufNewFile,BufRead */.content.xml setlocal tabstop=2
augroup END
augroup TypeXMLMaven
  autocmd!
  autocmd BufNewFile,BufRead */pom.xml setlocal expandtab
  autocmd BufNewFile,BufRead */pom.xml setlocal shiftwidth=4
  autocmd BufNewFile,BufRead */pom.xml setlocal softtabstop=4
  autocmd BufNewFile,BufRead */pom.xml setlocal tabstop=4
augroup END
augroup TypeSCSS
  autocmd!
  " https://github.com/cakebaker/scss-syntax.vim
  " autocmd BufRead,BufNewFile *.scss set filetype=scss.css
  autocmd FileType scss setlocal iskeyword+=-
augroup END
augroup TypeNodeJsConfig
  autocmd!
  autocmd BufRead,BufNewFile .babelrc set filetype=json
  autocmd BufRead,BufNewFile .eslintrc set filetype=json
  autocmd BufRead,BufNewFile .prettierrc set filetype=json
  autocmd BufRead,BufNewFile .stylelintrc set filetype=json
augroup END
augroup TypeZsh
  autocmd!
  " tell shellcheck zsh is bash; for the lints
  autocmd BufNewFile,BufRead .zlogin,.zpreztorc,.zprofile,.zshenv,.zshrc,*.zsh,*.zsh-theme let g:is_bash=1
  autocmd BufNewFile,BufRead .zlogin,.zpreztorc,.zprofile,.zshenv,.zshrc,*.zsh,*.zsh-theme setlocal filetype=sh
augroup END
" |- }}}
" |o backups
" |- {{{
set backupdir=~/.config/nvim/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.config/nvim/tmp
" |- }}}
" |o key bindings
" |- {{{
" |>- buffers/"tabs"
" |>- {{{
" replace :tabnew
nmap <leader>T :enew<cr>
" move to the next buffer
nmap <leader>l :bnext<cr>
" move to previous buffer
nmap <leader>h :bprevious<cr>
" close current buffer and move to previous buffer
nmap <leader>bq :bp <bar> bd #<cr>
" show all open buffers
nmap <leader>bl :ls<cr>
" |>- }}}
" |>- diff
" |>- {{{
if &diff
    map ] ]c
    map [ [c
endif
" |>- }}}
" |>- folding
" |>- {{{
" \ft recursive folds toggle
nnoremap <leader>ft zA
" <space> toggle fold
nnoremap <space> za
set foldenable          " fold files by default
set foldmethod=syntax   " fold based on syntax
set foldnestmax=10      " max 10 depth
set foldlevelstart=4    " start folds at 4th depth level
" |>- }}}
" |- }}}
" |o misc
" |- {{{
set cursorline " highlight current line
set number     " show line numbers
set showcmd    " show command in bottom bar
set showmatch  " higlight matching parenthesis
" |>- searching
set hlsearch   " highlight all matches
" |>-
" |>- visual mode
" |>- {{{
" allow visual block to go past shortest line
set virtualedit=
set wildmenu
set laststatus=2
set wrap linebreak nolist
set wildmode=full
" |>- }}}
" |>- whitespace
" |>- {{{
set cindent       " c-style autoindent
set expandtab     " use spaces for tabs
set modelines=1
set softtabstop=2 " 2 space tab
set shiftwidth=2
set tabstop=2     " 2 space tab
" |>- }}}
" |- }}}
" * }}}
" inspired by:
" :h init.vim
" http://joshldavis.com/2014/04/05/vim-tab-madness-buffers-vs-tabs/
" *
" vim:foldmethod=marker:foldlevel=1
