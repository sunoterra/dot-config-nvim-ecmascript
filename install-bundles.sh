#!/usr/bin/env bash
_pwd=$(pwd)

# required directories
_autoload="autoload"
_colors="colors"
_neosolarized="${_colors}/NeoSolarized.vim"
_pathogen="${_autoload}/pathogen.vim"

directory_exists_for() {
  echo " ✓ \"${1}\" directory exists"
}

symlink_exists_for() {
  echo " ✓ \"${1}\" symlink exists"
}

cd "${_pwd}/bundle" || exit 0
echo "↳ change to ${_pwd}/bundle"

while IFS= read -r bundle
do
  plugin=$(echo "${bundle}" | cut -d "/" -f 5)
  if [[ -d "${plugin}" ]]
  then
    directory_exists_for "${plugin}"
  else
    echo " > git clone ${bundle}" && git clone "${bundle}"
  fi
done < "../bundles.dat"

cd "${_pwd}" || exit 0
echo "↰ ${_pwd}"

# install pathogen
if [[ -d "${_autoload}" ]]
then
  directory_exists_for "${_autoload}"
else
  mkdir -v "${_autoload}"
fi
if [[ -h "${_pathogen}" ]]
then
  symlink_exists_for "${_pathogen}"
else
  ln -sv "bundle/vim-pathogen/${_pathogen}" "${_pathogen}"
fi

# install NeoSolarized
if [[ -d "${_colors}" ]]
then
  directory_exists_for "${_colors}"
else
  mkdir -v "${_colors}"
fi
if [[ -h "${_neosolarized}" ]]
then
  symlink_exists_for "${_neosolarized}"
else
  ln -sv "bundle/NeoSolarized/${_neosolarized}" "${_neosolarized}"
fi

unset _autoload
unset _colors
unset _neosolarized
unset _pathogen
unset _pwd
